package fr.colyndylan.desperadovirus.main;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

public class MainApplication implements CommandLineRunner {

  /**
   * Starting point of application
   * 
   * @param args
   */
  public static void main(String[] args) {
    SpringApplication.run(MainApplication.class, args);
  }

  /**
   * Starting run point
   */
  @Override
  public void run(String... args) {

  }
}
