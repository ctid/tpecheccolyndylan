package fr.colyndylan.desperadovirus.entities;

public class Board {
  private Piece[][] board;
  private boolean isWhiteTurn;

  /**
   * get actual board
   * 
   * @return board with pieces
   */
  public Piece[][] getBoard() {
    return board;
  }

  /**
   * set new board
   * 
   * @param board new board
   */
  public void setBoard(Piece[][] board) {
    this.board = board;
  }

  /**
   * get is white turn to play
   * 
   * @return isWhiteTurn
   */
  public boolean isWhiteTurn() {
    return isWhiteTurn;
  }

  /**
   * set whos turn
   * 
   * @param isWhiteTurn whos turn
   */
  public void setWhiteTurn(boolean isWhiteTurn) {
    this.isWhiteTurn = isWhiteTurn;
  }

  /**
   * Initialize board with all pieces
   */
  public void initialize() {

    board = new Piece[8][8];
    int lineBlack = 0;
    int linePawnBlack = 1;
    int lineWhite = 7;
    int linePawnWhite = 6;

    for (int line = 0; line < board.length; line++) {
      for (int column = 0; column < board[line].length; column++) {
        if (line == lineBlack) {
          switch (column) {
            case 0:
              board[line][column] = new Rook(false);
              break;
            case 1:
              board[line][column] = new Knight(false);
              break;
            case 2:
              board[line][column] = new Bishop(false);
              break;
            case 3:
              board[line][column] = new Queen(false);
              break;
            case 4:
              board[line][column] = new King(false);
              break;
            case 5:
              board[line][column] = new Bishop(false);
              break;
            case 6:
              board[line][column] = new Knight(false);
              break;
            case 7:
              board[line][column] = new Rook(false);
              break;
            default:
              board[line][column] = null;
              break;
          }
        } else if (line == linePawnBlack) {
          board[line][column] = new Pawn(false);
        } else if (line == linePawnWhite) {
          board[line][column] = new Pawn(true);
        } else if (line == lineWhite) {
          switch (column) {
            case 0:
              board[line][column] = new Rook(true);
              break;
            case 1:
              board[line][column] = new Knight(true);
              break;
            case 2:
              board[line][column] = new Bishop(true);
              break;
            case 3:
              board[line][column] = new King(true);
              break;
            case 4:
              board[line][column] = new Queen(true);
              break;
            case 5:
              board[line][column] = new Bishop(true);
              break;
            case 6:
              board[line][column] = new Knight(true);
              break;
            case 7:
              board[line][column] = new Rook(true);
              break;
            default:
              board[line][column] = null;
              break;
          }
        }
      }
    }
  }

  /**
   * print the board to the console
   * 
   * @param isWhite is white turn
   * @return boolean to know if board is showed
   */
  public boolean showBoard(boolean isWhite) {
    boolean isFinished = false;
    String separator = "------------------------------------------------------------------";

    try {
      if (isWhite) {
        System.out.println("\ta\tb\tc\td\te\tf\tg\th");
        System.out.println(separator);
        for (int line = 0; line < board.length; line++) {
          System.out.print(8 - line + "|\t");
          for (int column = 0; column < board[line].length; column++) {
            if (board[line][column] != null) {
              System.out.print(board[line][column]);
              System.out.print("\t");

            } else {
              System.out.print("\t");

            }
          }
          System.out.println();
        }
        System.out.println(separator);
        isFinished = true;
      } else {
        System.out.println("\th\tg\tf\te\td\tc\tb\ta");
        System.out.println(separator);
        for (int line = board.length; line > 0; line--) {
          System.out.print(9 - line + "|\t");
          for (int column = board[line - 1].length; column > 0; column--) {
            if (board[line - 1][column - 1] != null) {
              System.out.print(board[line - 1][column - 1]);
              System.out.print("\t");

            } else {
              System.out.print("\t");

            }
          }
          
          System.out.println();
        }
        System.out.println(separator);
        isFinished = true;
      }
    } catch (Exception e) {
      isFinished = false;
      throw(e);
    }

    return isFinished;
  }

  /**
   * Check if piece movement is valid
   * 
   * @param startLine   entry line
   * @param startColumn entry column
   * @param endLine     arrival line
   * @param endColumn   arrival column
   * @return true if movement valid
   */
  public boolean validMovement(int startLine, int startColumn, int endLine, int endColumn) {

    if (startLine < 0 || startLine > 7 || startColumn < 0 || startColumn > 7 || endLine < 0 || endLine > 7
        || endColumn < 0 || endColumn > 7) {
      System.out.println("Le mouvement est en dehors de l'echiquier");
      return false;
    }

    if (board[startLine][startColumn] == null) {
      System.err.println("Il n'y a pas de piece a cet endroit");
      return false;
    }
    if (board[startLine][startColumn].isWhite != isWhiteTurn()) {
      System.err.println("Cette piece n'est pas a vous");
      return false;
    }
    if (!board[startLine][startColumn].validMovement(startLine, startColumn, endLine, endColumn)) {
      return false;
    }

    if (board[endLine][endColumn] == null) {
      if (board[startLine][startColumn] instanceof Pawn && startColumn != endColumn) {
        System.out.println("Le pion ne peut pas se deplacer en diagonale");
        return false;
      }
      int moveX = 0;
      int moveY = 0;

      if (startLine < endLine) {
        moveX = 1;
      } else if (startLine > endLine) {
        moveX = -1;
      }

      if (startColumn < endColumn) {
        moveY = 1;
      } else if (startColumn > endColumn) {
        moveY = -1;
      }

      int i = startLine;
      int j = startColumn;

      while (i != endLine || j != endColumn) {
        if (i != endLine) {
          i = i + moveX;
        }
        if (j != endColumn) {
          j = j + moveY;
        }
        if (board[i][j] != null) {
          System.err.println("Impossible de traverser une autre piece");
          return false;
        }
      }
      return true;
    }

    if (board[startLine][startColumn] instanceof Pawn && startColumn == endColumn) {
      System.err.println("Une autre piece bloque votre pion, il ne peut pas avancer");
      return false;
    }

    if (board[startLine][startColumn].isWhite && board[endLine][endColumn].isWhite) {
      System.err.println("Vous ne pouvez pas manger un pion de votre equipe");
      return false;
    }

    return true;
  }
}
