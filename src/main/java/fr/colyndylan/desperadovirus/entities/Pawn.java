package fr.colyndylan.desperadovirus.entities;

import fr.colyndylan.desperadovirus.constants.PieceCode;

public class Pawn extends Piece {

  /**
   * Constructor
   * 
   * @param isWhite whos turn
   */
  public Pawn(boolean isWhite) {
    super(isWhite);
    this.symbole = this.isWhite ? PieceCode.WHITE_PAWN : PieceCode.BLACK_PAWN;
  }

  @Override
  public boolean validMovement(int startLine, int startColumn, int endLine, int endColumn) {
    if (!isWhite) {
      if ((startColumn == endColumn) && endLine == (startLine + 1)) {
        return true;
      }
      if ((startLine == 6) && (startColumn == endColumn) && (endLine == (startLine + 2))) {
        return true;
      }
      if ((endLine == (startLine + 1)) && (Math.abs(startColumn - endColumn) == 1)) {
        return true;
      }

    } else {
      if ((startColumn == endColumn) && endLine == (startLine - 1)) {
        return true;
      }
      if ((startLine == 1) && (startColumn == endColumn) && (endLine == (startLine - 2))) {
        return true;
      }
      if ((endLine == (startLine - 1)) && (Math.abs(startColumn - endColumn) == 1)) {
        return true;
      }
    }
    return false;
  }
}
