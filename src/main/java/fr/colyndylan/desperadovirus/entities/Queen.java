package fr.colyndylan.desperadovirus.entities;

import fr.colyndylan.desperadovirus.constants.PieceCode;

public class Queen extends Piece {

  /**
   * Constructor
   * 
   * @param isWhite whos turn
   */
  public Queen(boolean isWhite) {
    super(isWhite);
    this.symbole = this.isWhite ? PieceCode.WHITE_QUEEN : PieceCode.BLACK_QUEEN;
  }

  @Override
  public boolean validMovement(int startLine, int startColumn, int endLine, int endColumn) {
    // TODO Auto-generated method stub
    return false;
  }
}
