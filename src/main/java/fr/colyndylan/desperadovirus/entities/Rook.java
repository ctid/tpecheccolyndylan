package fr.colyndylan.desperadovirus.entities;

import fr.colyndylan.desperadovirus.constants.PieceCode;

public class Rook extends Piece {

  /**
   * Constructor
   * 
   * @param isWhite whos turn
   */
  public Rook(boolean isWhite) {
    super(isWhite);
    this.symbole = this.isWhite ? PieceCode.WHITE_ROOK : PieceCode.BLACK_ROOK;
  }

  @Override
  public boolean validMovement(int startLine, int startColumn, int endLine, int endColumn) {
    // TODO Auto-generated method stub
    return false;
  }
}
