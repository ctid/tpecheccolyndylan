package fr.colyndylan.desperadovirus.entities;

public abstract class Piece {

  boolean isWhite;
  String symbole;

  /**
   * return isWhite
   * 
   * @return true if piece is white
   */
  public boolean isWhite() {
    return isWhite;
  }

  /**
   * set is white piece
   * 
   * @param isWhite isWhite
   */
  public void setWhite(boolean isWhite) {
    this.isWhite = isWhite;
  }

  /**
   * return symbol of piece
   * 
   * @return symbol of piece
   */
  public String getSymbole() {
    return symbole;
  }

  /**
   * set piece symbol
   * 
   * @param symbole unicode symbol
   */
  public void setSymbole(String symbole) {
    this.symbole = symbole;
  }

  /**
   * Constructor
   * 
   * @param isWhite whos turn
   */
  public Piece(boolean isWhite) {
    this.isWhite = isWhite;
  }

  @Override
  public String toString() {
    return symbole;
  }

  /**
   * method to know if movement is valid 
   * @param startLine entry line
   * @param startColumn entry column
   * @param endLine arrival line
   * @param endColumn arrival column
   * @return true if movement is valid
   */
  public abstract boolean validMovement(int startLine, int startColumn, int endLine, int endColumn);
}
