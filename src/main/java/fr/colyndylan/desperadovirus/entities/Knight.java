package fr.colyndylan.desperadovirus.entities;

import fr.colyndylan.desperadovirus.constants.PieceCode;

public class Knight extends Piece {

  /**
   * Constructor
   * 
   * @param isWhite whos turn
   */
  public Knight(boolean isWhite) {
    super(isWhite);
    this.symbole = this.isWhite ? PieceCode.WHITE_KNIGHT : PieceCode.BLACK_KNIGHT;
  }

  @Override
  public boolean validMovement(int startLine, int startColumn, int endLine, int endColumn) {
    // TODO Auto-generated method stub
    return false;
  }
}
