package fr.colyndylan.desperadovirus.entities;

import fr.colyndylan.desperadovirus.constants.PieceCode;

public class King extends Piece {

  /**
   * Constructor
   * 
   * @param isWhite whos turn
   */
  public King(boolean isWhite) {
    super(isWhite);
    this.symbole = this.isWhite ? PieceCode.WHITE_KING : PieceCode.BLACK_KING;
  }

  @Override
  public boolean validMovement(int startLine, int startColumn, int endLine, int endColumn) {
    // TODO Auto-generated method stub
    return false;
  }
}
