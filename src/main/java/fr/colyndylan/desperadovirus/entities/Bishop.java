package fr.colyndylan.desperadovirus.entities;

import fr.colyndylan.desperadovirus.constants.PieceCode;

/**
 * Entity to manage Bishop
 */
public class Bishop extends Piece {

  /**
   * Bishop constructor
   * 
   * @param isWhite is true if piece is White
   */
  public Bishop(boolean isWhite) {
    super(isWhite);
    this.symbole = this.isWhite ? PieceCode.WHITE_BISHOP : PieceCode.BLACK_BISHOP;
  }

  @Override
  public boolean validMovement(int startLine, int startColumn, int endLine, int endColumn) {
    // TODO Auto-generated method stub
    return false;
  }

}
