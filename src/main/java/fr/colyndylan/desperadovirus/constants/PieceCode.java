package fr.colyndylan.desperadovirus.constants;

public class PieceCode {

  public final static String BLACK_KING = "\u265A";
  public final static String BLACK_QUEEN = "\u265B";
  public final static String BLACK_ROOK = "\u265C";
  public final static String BLACK_BISHOP = "\u265D";
  public final static String BLACK_KNIGHT = "\u265E";
  public final static String BLACK_PAWN = "\u265F";
  public final static String WHITE_KING = "\u2654";
  public final static String WHITE_QUEEN = "\u2655";
  public final static String WHITE_ROOK = "\u2656";
  public final static String WHITE_BISHOP = "\u2657";
  public final static String WHITE_KNIGHT = "\u2658";
  public final static String WHITE_PAWN = "\u2659";

}
