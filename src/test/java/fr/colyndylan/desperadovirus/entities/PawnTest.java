package fr.colyndylan.desperadovirus.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;

import fr.colyndylan.desperadovirus.entities.Board;
import fr.colyndylan.desperadovirus.entities.Pawn;
import fr.colyndylan.desperadovirus.entities.Piece;

import org.junit.jupiter.api.Test;

public class PawnTest {

  @Test
  public void testMovementValidInEmptySquare() {
    boolean isValid = false;

    Board board = new Board();

    board.initialize();
    board.showBoard(true);

    int startLine = 1; // 1 car index 1 = deuxième ligne
    int startColumn = 0; // 0 car index 0 = colonne a
    int endLine = 2; // 1 car index 2 = troisième ligne
    int endColumn = 0; // 0 car index 0 = colonne a

    isValid = board.validMovement(startLine, startColumn, endLine, endColumn);
    assertEquals(true, isValid);
  }

  @Test
  public void testMovementInvalidInOccupiedSquareForward() {
    boolean isValid = false;

    Board board = new Board();

    board.initialize();
    board.showBoard(true);

    int startLine = 1; // 1 car index 1 = deuxième ligne
    int startColumn = 0; // 0 car index 0 = colonne a
    int endLine = 2; // 1 car index 2 = troisième ligne
    int endColumn = 0; // 0 car index 0 = colonne a

    Piece[][] actualBoard = board.getBoard();
    actualBoard[endLine][endColumn] = new Pawn(false);
    board.setBoard(actualBoard);

    isValid = board.validMovement(startLine, startColumn, endLine, endColumn);
    assertEquals(false, isValid);
  }

  @Test
  public void testMovementValidInOccupiedSquareDiagonal() {
    boolean isValid = false;

    Board board = new Board();

    board.initialize();
    board.showBoard(true);

    int startLine = 1; // 1 car index 1 = deuxième ligne
    int startColumn = 0; // 0 car index 0 = colonne a
    int endLine = 2; // 1 car index 2 = troisièmeligne
    int endColumn = 1; // 0 car index 0 = colonne a

    Piece[][] actualBoard = board.getBoard();
    actualBoard[endLine][endColumn] = new Pawn(false);
    board.setBoard(actualBoard);

    isValid = board.validMovement(startLine, startColumn, endLine, endColumn);
    assertEquals(true, isValid);
  }

  @Test
  public void testMovementInvalidIllegal() {
    boolean isValid = false;

    Board board = new Board();

    board.initialize();
    board.showBoard(true);

    int startLine = 1; // 1 car index 1 = deuxième ligne
    int startColumn = 0; // 0 car index 0 = colonne a
    int endLine = 0; // 0 car index 0 = première ligne
    int endColumn = 0; // 0 car index 0 = colonne a

    isValid = board.validMovement(startLine, startColumn, endLine, endColumn);
    assertEquals(false, isValid);
  }

  @Test
  public void testMovementInvalidOutside() {
    boolean isValid = false;

    Board board = new Board();

    board.initialize();
    board.showBoard(true);

    int startLine = 1; // 1 car index 1 = deuxième ligne
    int startColumn = 0; // 0 car index 0 = colonne a
    int endLine = 0; // 1 car index 2 = troisième ligne
    int endColumn = 0; // 0 car index 0 = colonne a

    isValid = board.validMovement(startLine, startColumn, endLine, endColumn);
    assertEquals(false, isValid);
  }

  @Test
  public void showBoardWhenMovementValidInEmptySquare() {
    boolean isValid = false;

    Board board = new Board();

    board.initialize();
    board.showBoard(true);
    board.setWhiteTurn(true);
    int startLine = 6; // 1 car index 1 = deuxième ligne
    int startColumn = 0; // 0 car index 0 = colonne a
    int endLine = 5; // 1 car index 2 = troisième ligne
    int endColumn = 0; // 0 car index 0 = colonne a

    if (board.validMovement(startLine, startColumn, endLine, endColumn)) {
      Piece[][] actualBoard = board.getBoard();
      actualBoard[endLine][endColumn] = actualBoard[startLine][startColumn];
      actualBoard[startLine][startColumn] = null;
      board.setBoard(actualBoard);
      board.showBoard(false);
    }
  }

  @Test
  public void showBoardWhenMovementValidInOccupiedSquareDiagonal() {
    boolean isValid = false;

    Board board = new Board();

    board.initialize();
    board.setWhiteTurn(true);

    int startLine = 6; // 1 car index 1 = deuxième ligne
    int startColumn = 0; // 0 car index 0 = colonne a
    int endLine = 5; // 1 car index 2 = troisièmeligne
    int endColumn = 1; // 0 car index 0 = colonne a

    Piece[][] actualBoard = board.getBoard();
    actualBoard[endLine][endColumn] = new Pawn(false);
    board.setBoard(actualBoard);
    board.showBoard(true);

    if (board.validMovement(startLine, startColumn, endLine, endColumn)) {
      actualBoard = board.getBoard();
      actualBoard[endLine][endColumn] = actualBoard[startLine][startColumn];
      actualBoard[startLine][startColumn] = null;
      board.setBoard(actualBoard);
      board.showBoard(false);
    }
  }
}
