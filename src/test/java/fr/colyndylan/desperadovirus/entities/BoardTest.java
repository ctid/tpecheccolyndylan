package fr.colyndylan.desperadovirus.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.colyndylan.desperadovirus.entities.*;

public class BoardTest {

  @Test
  public void testInitializeBoard() {
    Board board = new Board();

    board.initialize();

    boolean isValid = true;

    for (int line = 0; line < board.getBoard().length; line++) {
      for (int column = 0; column < board.getBoard()[line].length; column++) {
        if (line == 0) {
          switch (column) {
          case 0:
            isValid = isValid && board.getBoard()[line][column] instanceof Rook;
            break;
          case 1:
            isValid = isValid && board.getBoard()[line][column] instanceof Knight;
            break;
          case 2:
            isValid = isValid && board.getBoard()[line][column] instanceof Bishop;
            break;
          case 3:
            isValid = isValid && board.getBoard()[line][column] instanceof Queen;
            break;
          case 4:
            isValid = isValid && board.getBoard()[line][column] instanceof King;
            break;
          case 5:
            isValid = isValid && board.getBoard()[line][column] instanceof Bishop;
            break;
          case 6:
            isValid = isValid && board.getBoard()[line][column] instanceof Knight;
            break;
          case 7:
            isValid = isValid && board.getBoard()[line][column] instanceof Rook;
            break;
          }
        } else if (line == 1) {
          isValid = isValid && board.getBoard()[line][column] instanceof Pawn;
        } else if (line == 6) {
          isValid = isValid && board.getBoard()[line][column] instanceof Pawn;
        } else if (line == 7) {
          switch (column) {
          case 0:
            isValid = isValid && board.getBoard()[line][column] instanceof Rook;
            break;
          case 1:
            isValid = isValid && board.getBoard()[line][column] instanceof Knight;
            break;
          case 2:
            isValid = isValid && board.getBoard()[line][column] instanceof Bishop;
            break;
          case 3:
            isValid = isValid && board.getBoard()[line][column] instanceof King;
            break;
          case 4:
            isValid = isValid && board.getBoard()[line][column] instanceof Queen;
            break;
          case 5:
            isValid = isValid && board.getBoard()[line][column] instanceof Bishop;
            break;
          case 6:
            isValid = isValid && board.getBoard()[line][column] instanceof Knight;
            break;
          case 7:
            isValid = isValid && board.getBoard()[line][column] instanceof Rook;
            break;
          }
        } else {
          isValid = isValid && board.getBoard()[line][column] == null;
        }
      }
    }

    assertEquals(true, isValid);
  }

  @Test
  public void testShowBoard() {
    Board board = new Board();

    board.initialize();

    assertEquals(true, board.showBoard(true));
  }

}
